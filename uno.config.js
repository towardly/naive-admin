import { defineConfig, presetAttributify, presetUno } from 'unocss';

export default defineConfig({
  content: {
    pipeline: {
      exclude: [
        'node_modules',
        '.git',
        '.github',
        '.husky',
        '.vscode',
        'build',
        'dist',
        'mock',
        'public',
        './stats.html',
      ],
    },
  },
  shortcuts: [['f-c-c', 'flex justify-center items-center']],
  presets: [presetUno(), presetAttributify()],
  rules: [
    [/^bc-(.+)$/, ([, color]) => ({ 'border-color': `#${color}` })],
    [/^bc1v-(.+)$/, ([, cssvar]) => ({ border: `1px solid var(--${cssvar})` })],
    [/^bc1-(.+)$/, ([, color]) => ({ border: `1px solid #${color}` })],
  ],
  theme: {
    colors: {
      primary: 'var(--primary-color)',
    },
  },
});
