import { createRouter, createWebHistory } from 'vue-router';
import { basicRoutes } from './routes';
import { setupRouterGuard } from './guard';

const router = createRouter({
  history: createWebHistory(),
  routes: basicRoutes,
});

/** 添加动态路由, 用于添加后台权限的路由 */
async function addDynamicRoutes() {}

export async function setupRouter(app) {
  await addDynamicRoutes(); // 添加接口传递的动态路由
  setupRouterGuard(router); // 设置路由守卫
  app.use(router);
}
