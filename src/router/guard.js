import { isBlank } from 'ph-utils';

/** 设置路由守卫 */
export function setupRouterGuard(router) {
  router.afterEach((to) => {
    /** 设置网页标题 */
    const meta = to.meta;
    if (meta != null) {
      const pageTitle = meta.title;
      if (!isBlank(pageTitle)) {
        document.title = pageTitle;
      }
    }
  });
}
