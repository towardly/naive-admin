import Layout from '@/Layout/index.vue';

/** 项目的基础路由 */
export const basicRoutes = [
  {
    name: 'WorkbenchLayout',
    path: '/',
    component: Layout,
    redirect: '/workbench',
    children: [
      {
        name: 'Workbench',
        path: 'workbench',
        component: () => import('@/views/workbench/index.vue'),
        meta: {
          title: '工作台',
        },
      },
    ],
  },
  {
    name: 'Login',
    path: '/login',
    component: () => import('@/views/login/index.vue'),
    meta: {
      title: '登录',
    },
  },
];
