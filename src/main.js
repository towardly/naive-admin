import 'phui-v/theme/base/vars.less';
import './styles/resset.css';
import 'virtual:uno.css';
import './styles/global.less';
import 'phui-v/style/message';
import 'phui-v/style/icon';

import { createApp } from 'vue';
import App from './App.vue';
import { setupRouter } from './router';

async function setupApp() {
  const app = createApp(App);
  await setupRouter(app); // 设置路由
  app.mount('#app');
}

setupApp();
